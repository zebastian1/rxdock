#!/usr/bin/php
<?php
echo <<<'EOD'
This file applies common formatting rules to RxDock source code files
(.cxx and .h) with the following rules:
- add new file header comment
- include guard changed to pragma once
- clang-format of the file
- remove of redundant comments
- clang-tidy cleanup modernize to c++-11

Usage: ./cleanup.php [FILE(S)]
Example: ./cleanup.php src/lib/RbtA*

The following steps should be done manually afterwards:
	improve readability
		remove unneccessary comments
        ...
	performance enhancements
	...

EOD;

$files = glob(@$argv[1]);
if(empty($files)) die('Error: no files matched arg: ' . @$argv[1] . PHP_EOL);

echo PHP_EOL . 'Starting format...' . PHP_EOL;
foreach($files as $file){
	echo 'Handling file: ' . $file . PHP_EOL;
	applyNewFileHeader($file);
	applyIncludeGuard($file);
	applyRemoveOfKnownRedundantComments($file);
	shell_exec('clang-format -i ' . $file);
	applyClangTidy($file);
}
echo 'Formatted ' . count($files) . ' file(s)' . PHP_EOL;
exit;

function applyNewFileHeader($file){
    $content = file_get_contents($file);
    $regex = '/^\/\*+\n\s*\*\s*[\S\s]*?\*\/(\n\n(?:\s*\/\/.*)+)?([\S\s]*)/';
    $newHeader = <<<EOD
//===-- __FILENAME__ - __SHORT_DESCRIPTION__ __SPACE_HEAD__----------*- C++ -*-===//
//
// Part of the RxDock project, under the GNU LGPL version 3.
// Visit https//www.rxdock.org/ for more information.
// Copyright (c) 1998--2006 RiboTargets (subsequently Vernalis (R&D) Ltd)
// Copyright (c) 2006--2012 University of York
// Copyright (c) 2012--2014 University of Barcelona
// Copyright (c) 2019--2020 RxTx
// SPDX-License-Identifier: LGPL-3.0-only
//
//===----------------------------------------------------------------------===//
///
/// \\file
/// __FILE_DESCRIPTION__
///
//===----------------------------------------------------------------------===//
EOD;
    if(preg_match($regex, $content, $match)){
		$possibleFileComment = $match[1];
		$otherFileContent = $match[2];
        $filename = basename($file);
		$fileDescription = 'TODO: long description';
		$shortDescription = 'TODO: short description';
		if(!empty($possibleFileComment)) $fileDescription = str_replace('//', '///', trim($possibleFileComment, "\n/ "));
		$newHeader = str_replace('__FILENAME__', ucfirst($filename), $newHeader);
		$newHeader = str_replace('__FILE_DESCRIPTION__', $fileDescription, $newHeader);
		$newHeader = str_replace('__SHORT_DESCRIPTION__', $shortDescription, $newHeader);
		$newHeader = str_replace('__SPACE_HEAD__',
            str_repeat('-', 43 - max(1, (strlen($filename) + strlen($shortDescription)))), $newHeader);
        $newFileContent = $newHeader . $otherFileContent;
        file_put_contents($file, $newFileContent);
    }
}

function applyIncludeGuard($file){
	$content = file_get_contents($file);
	$defineRegex = '/^([\s\S]*?)#ifndef\s(_RBT[\s\S]+?)(\n#define\s)(\S+)([\S\s]+)(#endif\s)[\s\S]+$/';
	if(preg_match($defineRegex, $content, $match)){
		$newFileContent = $match[1] . '#pragma once' . $match[5];
		file_put_contents($file, $newFileContent);
	}
}


function applyCLangTidy($file){
	$checks = array(
		'modernize-avoid-bind',
		'modernize-deprecated-headers',
		'modernize-loop-convert',
		'modernize-make-shared',
		'modernize-make-unique',
		'modernize-pass-by-value',
		'modernize-raw-string-literal',
		'modernize-redundant-void-arg',
		'modernize-replace-auto-ptr',
		'modernize-replace-random-shuffle',
		'modernize-return-braced-init-list',
		'modernize-shrink-to-fit',
		'modernize-unary-static-assert',
		// 'modernize-use-auto',
		'modernize-use-bool-literals',
		// 'modernize-use-default-member-init',
		'modernize-use-emplace',
		'modernize-use-equals-default',
		'modernize-use-equals-delete',
		'modernize-use-noexcept',
		'modernize-use-nullptr',
		'modernize-use-override',
		'modernize-use-transparent-functors',
		//'modernize-use-using',
	);
	$checkString = '-*,' . implode(',', $checks);
  /*$headerIncludes = array(
    'include',
    'include/GP',
    'pcg-prefix/src/pcg/include',
    trim(shell_exec('pkg-config --cflags-only-I eigen3')),
    '/usr/include/c++/7',
    '/usr/include/x86_64-linux-gnu/c++/7'
  );


  $cmd = 'clang-tidy'
    . ' -checks ' . $checkString
    . ' -header-filter="include/GP/*"'
    . ' -p . ' . $file
    . ' -fix'
    . ' -- -isystem ' . implode(' -isystem ', $headerIncludes);*/

  shell_exec('cmake CMakeLists.txt -DCMAKE_EXPORT_COMPILE_COMMANDS=ON');

  $cmd = 'run-clang-tidy-6.0.py'
  . ' -checks=' . $checkString
  . ' -p .'
  . ' -clang-tidy-binary clang-tidy-5.0'
  . ' -clang-apply-replacements-binary clang-apply-replacements-5.0'
  . ' -fix'
  . ' -header-filter include'
  . ' ' . $file;

  // die($cmd);

	shell_exec($cmd);
}

function applyRemoveOfKnownRedundantComments($file){

    $content = file_get_contents($file);
    $commentSingleLines = array('Smart pointer', 'Vector of smart pointers',
        'Useful typedefs');
    foreach($commentSingleLines as $commentSingleLine){
        $content = str_replace('// ' . $commentSingleLine, '', $content);
    }
    $commentMultilineBlocks = array('Constructors', 'Destructor',
        'Public methods', 'Private methods', 'Protected methods',
        'Public data', 'Private data', 'Protected data',
        'Constructors\/destructors',
    );
    foreach($commentMultilineBlocks as $commentMultilineBlock){
        $content = preg_replace('/\n\s*\/\/*\n\s*\/\/*\s*' . $commentMultilineBlock . '\s*\n\s*\/\/*/', '', $content);
        $content = preg_replace('/\n\s*\/*\n\s*\/*\s*' . $commentMultilineBlock . '\s*\n/', "\n", $content);
    }
    $visibilityFlags = array('private', 'protected', 'public');
    foreach($visibilityFlags as $visibilityFlag1){
        foreach($visibilityFlags as $visibilityFlag2){
            $content = preg_replace('/\n' . $visibilityFlag1 . ':\n' . $visibilityFlag2 . ':\n/', $visibilityFlag2 . ":\n", $content);
        }
    }
    file_put_contents($file, $content);
}
