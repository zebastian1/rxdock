//===-- Rbt.cxx - Misc non-member functions in Rbt namespace ----*- C++ -*-===//
//
// Part of the RxDock project, under the GNU LGPL version 3.
// Visit https//www.rxdock.org/ for more information.
// Copyright (c) 1998--2006 RiboTargets (subsequently Vernalis (R&D) Ltd)
// Copyright (c) 2006--2012 University of York
// Copyright (c) 2012--2014 University of Barcelona
// Copyright (c) 2019--2020 RxTx
// SPDX-License-Identifier: LGPL-3.0-only
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Misc non-member functions in Rbt namespace
///
//===----------------------------------------------------------------------===//

#pragma once

#include <map>
#include <vector>

#include "RbtError.h"

#if defined _WIN32 || defined __CYGWIN__
#ifdef __GNUC__
#define RBTDLL_EXPORT __attribute__((dllexport))
#define RBTDLL_IMPORT __attribute__((dllimport))
#else
#define RBTDLL_EXPORT __declspec(dllexport)
#define RBTDLL_IMPORT __declspec(dllimport)
#endif
#define RBTDLL_LOCAL
#else
#if __GNUC__ >= 4
#define RBTDLL_EXPORT __attribute__((visibility("default")))
#define RBTDLL_IMPORT __attribute__((visibility("default")))
#define RBTDLL_LOCAL __attribute__((visibility("hidden")))
#else
#define RBTDLL_EXPORT
#define RBTDLL_IMPORT
#define RBTDLL_LOCAL
#endif
#endif

/** Segment is a named part of an RbtModel (usually an intact molecule)
 * For now, a segment is defined as just an std::string */
typedef std::string RbtSegment;

/** RbtSegmentMap holds a std::map of<br>
 * key = unique segment name<br>
 * value = number of atoms in segment */
typedef std::map<RbtSegment, unsigned int> RbtSegmentMap;
typedef RbtSegmentMap::iterator RbtSegmentMapIter;
typedef RbtSegmentMap::const_iterator RbtSegmentMapConstIter;

namespace Rbt {

/** @name Resource Handling Functions */
///@{

/** @return value of RBT_ROOT env variable */
std::string GetRbtRoot();
/** @return value of RBT_HOME env variable (or HOME if RBT_HOME is undefined)
 * If HOME is undefined, returns current working directory */
std::string GetRbtHome();
/** @return program name */
RBTDLL_EXPORT std::string GetProgramName();
/** @return legalese statement */
std::string GetCopyright();
/** @return current library version */
RBTDLL_EXPORT std::string GetVersion();
/** @return library product name */
RBTDLL_EXPORT std::string GetProduct();
/** @return current time and date as an std::string */
std::string GetTime();
/** @return current working directory */
RBTDLL_EXPORT std::string GetCurrentWorkingDirectory();

///@}
/** @name File/Directory Handling Functions */
///@{

/**
 * @return the full path to a subdirectory in the rDock directory structure
 *
 * @param strSubdir the relative path of the folder for example "data"
 *
 * If RBT_ROOT environment variable is ~dave/ribodev/molmod/ribodev
 * then this method would return ~dave/ribodev/molmod/ribodev/data/
 *
 * If RBT_ROOT is not set, then GetRbtDirName returns ./ irrespective of the
 * subdirectory asked for. Thus the fall-back position is that parameter files
 * are read from the current directory if RBT_ROOT is not defined. */
std::string GetRbtDirName(const std::string &strSubdir = "");

/** @return the full path to a file in the rDock directory structure
 *
 * First check if the file exists in the CWD, if so return this path
 * Next check RBT_HOME directory, if so return this path
 * Finally, return the path to the file in the rDock directory structure
 * (without checking if the file is actually present)
 * */
RBTDLL_EXPORT std::string GetRbtFileName(const std::string &strSubdir,
                                         const std::string &strFile);

/** @return the string following the last "." in the file name.
 *
 * "receptor.psf" would yield "psf"<br>
 * If no "." is present, returns the whole file name
 * */
std::string GetFileType(const std::string &strFile);

/** @return a list of files in a directory matching criteria
 * @param strDir consider files inside this directory
 * @param strFilePrefix (optional) consider files beginning with this prefix
 * @param strFileType (optional) consider files of this type
 * (as returned by GetFileType)
 */
std::vector<std::string> GetDirList(const std::string &strDir,
                                    const std::string &strFilePrefix = "",
                                    const std::string &strFileType = "");
///@}
/** @name Conversion Routines */
///@{

/** @return converted (comma)-delimited string of segment names to segment map
 */
RBTDLL_EXPORT RbtSegmentMap ConvertStringToSegmentMap(
    const std::string &strSegments, const std::string &strDelimiter = ",");

/** @return (comma)-delimited string of segment names converted from segment map
 */
std::string ConvertSegmentMapToString(const RbtSegmentMap &segmentMap,
                                      const std::string &strDelimiter = ",");

/** @return a segment map containing the members of map1 which are not in map2
 * TODO: port to stl or template method */
RbtSegmentMap SegmentDiffMap(const RbtSegmentMap &map1,
                             const RbtSegmentMap &map2);

/** @return converted (comma)-delimited string to string list (similar to
 * ConvertStringToSegmentMap, but returns list not map) */
RBTDLL_EXPORT std::vector<std::string>
ConvertDelimitedStringToList(const std::string &strValues,
                             const std::string &strDelimiter = ",");

/** @return converted string list to (comma)-delimited string (inverse of
 * ConvertDelimitedStringToList) */
std::string
ConvertListToDelimitedString(const std::vector<std::string> &listOfValues,
                             const std::string &strDelimiter = ",");

/** @return detected terminal width and wrap text to that width */
std::string WrapTextToTerminalWidth(const std::string &text);

///@}
/** @name I/O Routines */
///@{

/**
 * Prints a standard header to an output stream (for log files etc)
 * Contains copyright info, library version, date, time, executable information
 * etc
 */
RBTDLL_EXPORT std::ostream &
PrintStdHeader(std::ostream &s, const std::string &strExecutable = "");

/**
 * Prints the Bibliography information for quoting this program academically
 */
RBTDLL_EXPORT std::ostream &
PrintBibliographyItem(std::ostream &s, const std::string &publicationKey = "");

///@}
/** @name Helper Functions
 *
 * Helper functions to read/write chars from iostreams
 * Throws error if stream state is not Good() before and after the read/write
 * It appears the STL std::ios_base exception throwing is not yet implemented
 * at least on RedHat 6.1, so this is a temporary workaround (yeah right) */
///@{

//#ifdef __sgi
/** DM 25 Sep 2000 - with MIPSPro CC compiler, streamsize is not defined,
 * at least with the iostreams library we are using, so typedef it here
 * it is not in gcc 3.2.1 either SJ */
typedef int streamsize;
//#endif

RBTDLL_EXPORT void WriteWithThrow(std::ostream &ostr, const char *p,
                                  streamsize n);
RBTDLL_EXPORT void ReadWithThrow(std::istream &istr, char *p, streamsize n);

/** Used to read RbtCoord. The separator between x y z should be a
 * ',', but this takes care of small mistakes, reading any white
 * space or commas there is between each variable.
 * If necessary, it can be modified to accept the ',' as a
 * parameter to be able to use it when other separators are
 * needed. Also, it should be possible to implement it as a
 * manipulator of std::istream. */
std::istream &eatSeps(std::istream &is);

///@}

} // namespace Rbt
