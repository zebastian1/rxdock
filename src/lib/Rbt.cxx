//===-- Rbt.cxx - Misc non-member functions in Rbt namespace ----*- C++ -*-===//
//
// Part of the RxDock project, under the GNU LGPL version 3.
// Visit https//www.rxdock.org/ for more information.
// Copyright (c) 1998--2006 RiboTargets (subsequently Vernalis (R&D) Ltd)
// Copyright (c) 2006--2012 University of York
// Copyright (c) 2012--2014 University of Barcelona
// Copyright (c) 2019--2020 RxTx
// SPDX-License-Identifier: LGPL-3.0-only
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Misc non-member functions in Rbt namespace
///
//===----------------------------------------------------------------------===//

#include <algorithm> //For sort
#include <climits>   //For PATH_MAX
#include <cstdlib>   //For getenv
#include <ctime>     //For time functions
#include <dirent.h>  //For directory handling
#include <fstream>   //For ifstream
#include <iomanip>   //For std::put_time
#include <sstream>   //For istringstream and ostringstream
#ifdef _WIN32
#include <direct.h>
#include <windows.h>   // GetConsoleScreenBufferInfo
#define getcwd _getcwd // CRT library getcwd
#else
#include <sys/ioctl.h> //For TIOCGWINSZ
#ifdef __sun
#include <termios.h>
#endif
#include <unistd.h> //For POSIX getcwd
#endif
//#include <ios>
#include "Rbt.h"
#include "RbtFileError.h"
#include "RbtResources.h"

std::string Rbt::GetRbtRoot() {
  char *szRbtRoot = std::getenv("RBT_ROOT");
  if (szRbtRoot != (char *)nullptr) {
    return std::string(szRbtRoot);
  } else {
    return GetCurrentWorkingDirectory();
  }
}

std::string Rbt::GetRbtHome() {
  char *szRbtHome = std::getenv("RBT_HOME");
  if (szRbtHome != (char *)nullptr) {
    return std::string(szRbtHome);
  } else {
    szRbtHome = std::getenv("HOME");
    if (szRbtHome != (char *)nullptr) {
      return std::string(szRbtHome);
    } else {
      return GetCurrentWorkingDirectory();
    }
  }
}

std::string Rbt::GetProgramName() { return IDS_NAME; }

std::string Rbt::GetCopyright() { return IDS_COPYRIGHT; }

std::string Rbt::GetVersion() { return IDS_VERSION; }

std::string Rbt::GetProduct() { return IDS_PRODUCT; }

std::string Rbt::GetTime() {
  std::time_t t = std::time(nullptr);       // Get time in seconds since 1970
  std::tm *pLocalTime = std::localtime(&t); // Convert to local time struct

  std::ostringstream oss;
  oss << std::put_time(pLocalTime, "%c");
  return oss.str(); // Convert to ascii string
}

std::string Rbt::GetCurrentWorkingDirectory() {
  std::string strCwd(".");
  char *szCwd = new char[PATH_MAX + 1]; // Allocate a temp char* array
  if (::getcwd(szCwd, PATH_MAX) != (char *)nullptr) { // Get the cwd
    strCwd = szCwd;
    // strCwd += "/";
  }
  delete[] szCwd; // Delete the temp array
  return strCwd;
}

std::string Rbt::GetRbtDirName(const std::string &strSubDir) {
  std::string strRbtDir = GetRbtRoot();
  if (!strSubDir.empty()) {
    strRbtDir += "/";
    strRbtDir += strSubDir;
  }
  return strRbtDir;
}

std::string Rbt::GetRbtFileName(const std::string &strSubdir,
                                const std::string &strFile) {
  // First see if the file exists in the current directory
  std::string strFullPathToFile(strFile);
  // Just open it, don't try and parse it (after all, we don't know what format
  // the file is)
  std::ifstream fileIn(strFullPathToFile.c_str(), std::ios_base::in);
  if (fileIn) {
    fileIn.close();
    return strFullPathToFile;
  } else {
    // DM 02 Aug 200 - check RBT_HOME directory
    strFullPathToFile = GetRbtHome() + "/" + strFile;
    // DM 27 Apr 2005 - under gcc 3.4.3 there are problems reusing the same
    // ifstream object after the first "file open" fails
    // fileIn.open(strFullPathToFile.c_str(),std::ios_base::in);
    std::ifstream fileIn2(strFullPathToFile.c_str(), std::ios_base::in);
    if (fileIn2) {
      fileIn2.close();
      return strFullPathToFile;
    } else {
      return GetRbtDirName(strSubdir) + "/" + strFile;
    }
  }
}

std::string Rbt::GetFileType(const std::string &strFile) {
  return strFile.substr(strFile.rfind('.') + 1);
}

std::vector<std::string> Rbt::GetDirList(const std::string &strDir,
                                         const std::string &strFilePrefix,
                                         const std::string &strFileType) {
  std::vector<std::string> dirList;

  // Check if we need to match on file prefix
  bool bMatchPrefix = !strFilePrefix.empty();
  // Check if we need to match on file type (suffix)
  bool bMatchType = !strFileType.empty();

  DIR *pDir = opendir(strDir.c_str()); // Open directory
  if (pDir != nullptr) {

    // DM 6 Dec 1999 - dirent_t appears to be SGI MIPSPro specific
    // At least on Linux g++, it is called dirent
#ifdef __sgi
    dirent_t *pEntry;
#else
    dirent *pEntry;
#endif

    while ((pEntry = readdir(pDir)) != nullptr) { // Read each entry
      std::string strFile = pEntry->d_name;
      if ((strFile == ".") || (strFile == ".."))
        continue; // Don't need to consider . and ..
      bool bMatch = true;
      // Eliminate those that don't match the prefix
      if (bMatchPrefix && (strFile.rfind(strFilePrefix, 0) != 0))
        bMatch = false;
      if (bMatchType &&
          (GetFileType(strFile) !=
           strFileType)) // Eliminate those that don't match the type
        bMatch = false;
      if (bMatch) // Only store the hits
        dirList.push_back(strFile);
    }
    closedir(pDir);
  }

  // Sort the file list into alphabetical order
  std::sort(dirList.begin(), dirList.end());

  return dirList;
}

RbtSegmentMap Rbt::ConvertStringToSegmentMap(const std::string &strSegments,
                                             const std::string &strDelimiter) {
#ifdef _DEBUG
  // std::cout << "ConvertStringToSegmentMap: " << strSegments << " delimiter="
  // << strDelimiter << std::endl;
#endif //_DEBUG

  std::string::size_type nDelimiterSize = strDelimiter.size();
  RbtSegmentMap segmentMap;

  // Check for null string or null delimiter
  if ((!strSegments.empty()) && (nDelimiterSize > 0)) {
    std::string::size_type iBegin =
        0; // indicies into string (sort-of iterators)
    std::string::size_type iEnd;
    // Not often a do..while loop is used, but in this case it's what we want
    // Even if no delimiter is present, we still need to extract the whole
    // string
    do {
      iEnd = strSegments.find(strDelimiter, iBegin);
#ifdef _DEBUG
      // std::cout << strSegments.substr(iBegin, iEnd-iBegin) << std::endl;
#endif //_DEBUG
      segmentMap[strSegments.substr(iBegin, iEnd - iBegin)] = 0;
      iBegin = iEnd + nDelimiterSize;
    } while (iEnd !=
             std::string::npos); // This is the check for delimiter not found
  }

  return segmentMap;
}

std::string Rbt::ConvertSegmentMapToString(const RbtSegmentMap &segmentMap,
                                           const std::string &strDelimiter) {
  std::string strSegments;

  // Check for empty segment map
  if (!segmentMap.empty()) {
    RbtSegmentMapConstIter iter = segmentMap.begin();
    strSegments += (*iter++).first; // Add first string
    // Now loop over remaining entries, adding delimiter before each string
    while (iter != segmentMap.end()) {
      strSegments += strDelimiter;
      strSegments += (*iter++).first;
    }
  }
  return strSegments;
}

RbtSegmentMap Rbt::SegmentDiffMap(const RbtSegmentMap &map1,
                                  const RbtSegmentMap &map2) {
  RbtSegmentMap map3 = map1; // Init return value to map1
  for (const auto &iter : map2)
    map3.erase(iter.first); // Now delete everything in map2
  return map3;
}

std::vector<std::string>
Rbt::ConvertDelimitedStringToList(const std::string &strValues,
                                  const std::string &strDelimiter) {
  std::string::size_type nDelimiterSize = strDelimiter.size();
  std::vector<std::string> listOfValues;

  // Check for null string or null delimiter
  if ((!strValues.empty()) && (nDelimiterSize > 0)) {
    std::string::size_type iBegin =
        0; // indicies into string (sort-of iterators)
    std::string::size_type iEnd;
    // Not often a do..while loop is used, but in this case it's what we want
    // Even if no delimiter is present, we still need to extract the whole
    // string
    do {
      iEnd = strValues.find(strDelimiter, iBegin);
      std::string strValue = strValues.substr(iBegin, iEnd - iBegin);
      listOfValues.push_back(strValue);
      iBegin = iEnd + nDelimiterSize;
    } while (iEnd !=
             std::string::npos); // This is the check for delimiter not found
  }
  return listOfValues;
}

std::string
Rbt::ConvertListToDelimitedString(const std::vector<std::string> &listOfValues,
                                  const std::string &strDelimiter) {
  std::string strValues;

  // Check for empty string list
  if (!listOfValues.empty()) {
    std::vector<std::string>::const_iterator iter = listOfValues.begin();
    strValues += *iter++; // Add first string
    // Now loop over remaining entries, adding delimiter before each string
    while (iter != listOfValues.end()) {
      strValues += strDelimiter;
      strValues += *iter++;
    }
  }
  return strValues;
}

std::string Rbt::WrapTextToTerminalWidth(const std::string &text) {
  size_t width = 80;

#ifdef _WIN32
  HANDLE handleStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
  if (handleStdOut != INVALID_HANDLE_VALUE) {
    CONSOLE_SCREEN_BUFFER_INFO csbInfo;
    GetConsoleScreenBufferInfo(handleStdOut, &csbInfo);
    width = static_cast<size_t>(csbInfo.dwSize.X);
  }
#else
  winsize winsz{};
  if ((ioctl(STDOUT_FILENO, TIOCGWINSZ, &winsz) == 0) && winsz.ws_col) {
    width = winsz.ws_col;
  }
#endif

  std::istringstream sourceText(text);
  std::ostringstream wrappedText;
  std::string word;

  // Handle non-empty strings word by word
  if (sourceText >> word) {
    wrappedText << word;
    size_t remaining = width - word.length();
    while (sourceText >> word) {
      if (remaining < word.length() + 1) {
        wrappedText << '\n' << word;
        remaining = width - word.length();
      } else {
        wrappedText << ' ' << word;
        remaining -= word.length() + 1;
      }
    }
  }
  return wrappedText.str();
}

std::ostream &Rbt::PrintStdHeader(std::ostream &s,
                                  const std::string &strExecutable) {
  s << "***********************************************" << std::endl;
  s << GetCopyright() << std::endl;
  if (!strExecutable.empty())
    s << "Executable:\t" << strExecutable << "/" << GetVersion() << std::endl;
  s << "Library:\t" << GetProduct() << "/" << GetVersion() << std::endl;
  s << "RBT_ROOT:\t" << GetRbtRoot() << std::endl;
  s << "RBT_HOME:\t" << GetRbtHome() << std::endl;
  s << "Current dir:\t" << GetCurrentWorkingDirectory() << std::endl;
  s << "Date:\t\t" << GetTime() << std::endl;
  s << "***********************************************" << std::endl;
  return s;
}

std::ostream &Rbt::PrintBibliographyItem(std::ostream &s,
                                         const std::string &publicationKey) {
  std::map<std::string, std::map<std::string, std::string>> publications = {
      {"RiboDock2004",
       {{"type", "article"},
        {"title", "Validation of an empirical RNA-ligand scoring function for "
                  "fast flexible docking using RiboDock\u00ae"},
        {"author", "Morley, S David and Afshar, Mohammad"},
        {"journal", "Journal of computer-aided molecular design"},
        {"volume", "18"},
        {"number", "3"},
        {"pages", "189--208"},
        {"year", "2004"},
        {"publisher", "Springer"}}},
      {"rDock2014",
       {{"type", "article"},
        {"title", "rDock: a fast, versatile and open source program for "
                  "docking ligands to proteins and nucleic acids"},
        {"author", "Ruiz-Carmona, Sergio and Alvarez-Garcia, Daniel and "
                   "Foloppe, Nicolas and Garmendia-Doval, A Beatriz and Juhos, "
                   "Szilveszter and Schmidtke, Peter and Barril, Xavier and "
                   "Hubbard, Roderick E and Morley, S David"},
        {"journal", "PLoS computational biology"},
        {"volume", "10"},
        {"number", "4"},
        {"pages", "e1003571"},
        {"year", "2014"},
        {"publisher", "Public Library of Science"}}},
      {"PCG2014",
       {{"type", "techreport"},
        {"title", "PCG: A Family of Simple Fast Space-Efficient Statistically "
                  "Good Algorithms for Random Number Generation"},
        {"author", "Melissa E. O'Neill"},
        {"institution", "Harvey Mudd College"},
        {"number", "HMC-CS-2014-0905"},
        {"year", "2014"},
        {"month", "Sep"},
        {"xurl", "https://www.cs.hmc.edu/tr/hmc-cs-2014-0905.pdf"}}},

  };
  std::string citation;
  citation += "Please cite the following ";
  citation += publications[publicationKey]["type"];
  citation += ": ";
  citation += publications[publicationKey]["author"];
  citation += ". ";
  citation += publications[publicationKey]["title"];
  citation += ". ";
  if (publications[publicationKey]["type"] == "article") {
    citation += publications[publicationKey]["journal"];
    citation += " ";
    citation += publications[publicationKey]["volume"];
    citation += "(";
    citation += publications[publicationKey]["number"];
    citation += "), ";
    citation += publications[publicationKey]["pages"];
  } else if (publications[publicationKey]["type"] == "techreport") {
    citation += publications[publicationKey]["xurl"];
  }
  citation += " (";
  citation += publications[publicationKey]["year"];
  citation += ").";
  s << WrapTextToTerminalWidth(citation) << std::endl;
  return s;
}

void Rbt::WriteWithThrow(std::ostream &ostr, const char *p, streamsize n) {
  if (!ostr)
    throw RbtFileWriteError(_WHERE_, "Error writing to output stream");
  ostr.write(p, n);
  if (!ostr)
    throw RbtFileWriteError(_WHERE_, "Error writing to output stream");
}

void Rbt::ReadWithThrow(std::istream &istr, char *p, streamsize n) {
  if (!istr)
    throw RbtFileReadError(_WHERE_, "Error reading from input stream");
  istr.read(p, n);
  if (!istr)
    throw RbtFileReadError(_WHERE_, "Error reading from input stream");
}

std::istream &eatSeps(std::istream &is) {
  char c;
  while (is.get(c)) {
    if (!isspace(c) && (c != ',')) {
      is.putback(c);
      break;
    }
  }
  return is;
}
