//===-- RbtOpenCL.cxx - Bootstrap Code to run on a opencl device *- C++ -*-===//
//
// Part of the RxDock project, under the GNU LGPL version 3.
// Visit https//www.rxdock.org/ for more information.
// Copyright (c) 1998--2006 RiboTargets (subsequently Vernalis (R&D) Ltd)
// Copyright (c) 2006--2012 University of York
// Copyright (c) 2012--2014 University of Barcelona
// Copyright (c) 2019--2020 RxTx
// SPDX-License-Identifier: LGPL-3.0-only
//
//===----------------------------------------------------------------------===//
///
/// \file
/// RbtOpenCL provides a listing functionility of the system detected opencl
/// contexts and devices. It also allows for static initialization of an opencl
/// runtime (init) and then use this runtime throughout the program to run
/// calculations on an opencl device (gpu calculation).
///
//===----------------------------------------------------------------------===//

#pragma once

#ifndef CL_TARGET_OPENCL_VERSION
#define CL_TARGET_OPENCL_VERSION 120
#endif

#if defined(__APPLE__) || defined(__MACOSX)
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif

class RbtOpenCL {
public:
	/**
	 * lists all platforms with all devices
	 * @return a string containing the information for cli output
	 */
	static std::string list();

	/**
	 * create a static instance of this class
	 * @param platformIndex opencl platform index (starting with 0)
	 * @param deviceIndex opencl device index (starting with 0)
	 */
	static void init(int platformIndex, int deviceIndex) {
		opencl = new RbtOpenCL(platformIndex, deviceIndex);
	}

	/**
	 * retrieve the static instance of this class for calculation usage
	 */
	static RbtOpenCL *get() { return opencl; }

	/**
	 * check whether opencl is usable (opencl has been initialized correctly)
	 */
	static bool available() {
		return opencl != nullptr ? opencl->isAvailable : false;
	}

	/**
	 * Run an opencl kernel
	 *
	 * This method is used to run a kernel code with the given buffers.
	 * required conventions of the kernelCode:
	 * - contains exactly one kernel method with the declaration "void kernel"
	 * - the kernel method takes first all input and then all output
	 *     parameters in the order of the given vectors.
	 * @param kernelCode The raw string of the kernel code
	 * @param input Input buffers to the kernel (readonly)
	 * @param output Output buffers of the kernel (writeonly)
	 * @param N The maximum index to run the kernel for
	 */
  bool run(std::string kernelCode, std::vector<std::pair<char *, int>> input,
           std::vector<std::pair<char *, int>> output, int N);

private:
  RbtOpenCL(int platformIndex, int deviceIndex);
  static RbtOpenCL *opencl;

  bool checkErr(cl_int err, std::string functionName);
  cl::Program getProgram(std::string kernelCode);
  void writeBinariesToCache(const std::string &hash,
                            const cl::Program &program);
  cl::Program loadBinariesFromCache(const std::string &hash);
  std::string getKernelCachePath(std::string hash);
  bool isAvailable;
  cl::Context context;
  cl::CommandQueue queue;
  std::vector<cl::Device> devices;
  cl::Device device;
  int platformIndex;
  int deviceIndex;
};

extern RbtOpenCL opencl;
