//===-- RbtOpenCL.cxx - Bootstrap Code to run on a opencl device *- C++ -*-===//
//
// Part of the RxDock project, under the GNU LGPL version 3.
// Visit https//www.rxdock.org/ for more information.
// Copyright (c) 1998--2006 RiboTargets (subsequently Vernalis (R&D) Ltd)
// Copyright (c) 2006--2012 University of York
// Copyright (c) 2012--2014 University of Barcelona
// Copyright (c) 2019--2020 RxTx
// SPDX-License-Identifier: LGPL-3.0-only
//
//===----------------------------------------------------------------------===//
///
/// \file
/// RbtOpenCL provides a listing functionility of the system detected opencl
/// contexts and devices. It also allows for static initialization of an opencl
/// runtime (init) and then use this runtime throughout the program to run
/// calculations on an opencl device (gpu calculation).
///
//===----------------------------------------------------------------------===//

#include "RbtOpenCL.h"
#include "Rbt.h"
#include <chrono>
#include <cstring>
#include <fstream>
#include <iostream>
#include <regex>

// TODO: auto detection and selection of first GPU, when no indices are given
RbtOpenCL *RbtOpenCL::opencl = nullptr;

RbtOpenCL::RbtOpenCL(int platformIndex, int deviceIndex) {
  this->platformIndex = platformIndex;
  this->deviceIndex = deviceIndex;

  // get platforms, e.g. AMD
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);
  isAvailable = false;
  if (platformIndex >= platforms.size()) {
    if (platforms.size() == 0)
      std::cout << " No OpenCL platforms found. Check installation!\n";
    else
      std::cout << " Platform index invalid: " << platformIndex << "\n";
    return;
  }
  cl::Platform platform = platforms[platformIndex];
  std::cout << "Using OpenCL platform: " << platform.getInfo<CL_PLATFORM_NAME>()
            << "\n";

  // get devices of the platform, eg: GPU RX480
  platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
  if (deviceIndex >= devices.size()) {
    if (platforms.size() == 0)
      std::cout << " No OpenCL devices found. Check installation!\n";
    else
      std::cout << " Device index invalid: " << deviceIndex << "\n";
    return;
  }
  device = devices[deviceIndex];
  std::cout << "Using OpenCL device: " << device.getInfo<CL_DEVICE_NAME>()
            << "\n";
  context = {device};
  queue = {context, device};
  isAvailable = true;
}

bool RbtOpenCL::run(std::string kernelCode,
                    std::vector<std::pair<char *, int>> input,
                    std::vector<std::pair<char *, int>> output, int N) {

  cl::Program program = getProgram(kernelCode);

  std::vector<cl::Buffer> inputBuffers;
  int err = 0;

  for (unsigned int i = 0; i < input.size(); i++) {
    inputBuffers.emplace_back(context, CL_MEM_WRITE_ONLY, input[i].second);
    err = queue.enqueueWriteBuffer(inputBuffers[i], CL_FALSE, 0,
                                   input[i].second, input[i].first);
    if (!checkErr(err, "handle input buffer: " + std::to_string(i)))
      return false;
  }
  std::vector<cl::Buffer> outputBuffers;
  for (unsigned int i = 0; i < output.size(); i++) {
    outputBuffers.emplace_back(context, CL_MEM_READ_ONLY, output[i].second);
    if (!checkErr(err, "handle output buffer: " + std::to_string(i)))
      return false;
  }
  std::regex re("kernel void (.*?)\\(");
  std::smatch match;
  std::string kernelName;
  if (std::regex_search(kernelCode, match, re) && match.size() >= 2) {
    kernelName = match.str(1);
  } else {
    std::cout << "cannot find kernel name in code!";
    return false;
  }
  cl::Kernel kernel(program, kernelName.c_str());
  // std::cout << "Kernel: '" << kernelName << "'\n";
  int arg = 0;
  for (unsigned int i = 0; i < input.size(); i++) {
    err = kernel.setArg(arg++, inputBuffers[i]);
    if (!checkErr(err, "set input arg: " + std::to_string(i)))
      return false;
  }
  for (unsigned int i = 0; i < output.size(); i++) {
    kernel.setArg(arg++, outputBuffers[i]);
    if (!checkErr(err, "set output arg: " + std::to_string(i)))
      return false;
  }
  err = queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(N),
                                   cl::NullRange);
  if (!checkErr(err, "enqueueNDRangeKernel"))
    return false;

  err = queue.finish();
  if (!checkErr(err, "finish"))
    return false;

  for (unsigned int i = 0; i < output.size(); i++) {
    int error = queue.enqueueReadBuffer(outputBuffers[i], CL_TRUE, 0,
                                        output[i].second, output[i].first);
    checkErr(error, "enqueueReadBuffer: " + std::to_string(i));
  }
  return true;
}

cl::Program RbtOpenCL::getProgram(std::string kernelCode) {
  std::string id = std::to_string(platformIndex) + "-" +
                   std::to_string(deviceIndex) + "-" + kernelCode;
  std::hash<std::string> hashFunction;
  std::string hash = std::to_string(hashFunction(id));
  std::ifstream file(getKernelCachePath(hash));
  cl::Program program;
  int err = 0;
  if (file.good()) {
    program = loadBinariesFromCache(hash);
  } else {
    cl::Program::Sources sources;
    sources.push_back({kernelCode.c_str(), kernelCode.length()});
    program = cl::Program(context, sources);
    err = program.build({device}, "-cl-fast-relaxed-math");
  }

  checkErr(err, "program build");
  if (err != CL_SUCCESS) {
    std::cout << std::string("Error building: ") +
                     program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) + "\n";
  }
  writeBinariesToCache(hash, program);
  return program;
}

std::string RbtOpenCL::list() {
  std::string out = "OpenCL platforms and devices\n";
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);
  if (platforms.size() == 0)
    out += "No platforms available\n";
  for (unsigned int i = 0; i < platforms.size(); i++) {
    out += "\n ";
    out += "platform " + std::to_string(i) + ": " +
           platforms[i].getInfo<CL_PLATFORM_NAME>() + "\n";
    std::vector<cl::Device> devices;
    platforms[i].getDevices(CL_DEVICE_TYPE_ALL, &devices);
    for (unsigned int j = 0; j < devices.size(); j++) {
      out += "   ";
      out += "`device " + std::to_string(j) + ": " +
             devices[j].getInfo<CL_DEVICE_NAME>() + "\n";
    }
  }
  return out;
}

bool RbtOpenCL::checkErr(cl_int err, std::string functionName) {
  if (err != CL_SUCCESS) {
    std::cout << "OpenCl ERROR: " << functionName << " (Error code: " << err
              << ")\n";
    return false;
  } else
    return true;
}

void RbtOpenCL::writeBinariesToCache(const std::string &hash,
                                     const cl::Program &program) {
  std::ofstream bfile(getKernelCachePath(hash), std::ios::binary);
  if (!bfile)
    return;
  std::vector<size_t> sizes = program.getInfo<CL_PROGRAM_BINARY_SIZES>();
  std::vector<char *> binaries = program.getInfo<CL_PROGRAM_BINARIES>();
  for (int i = 0; i < sizes.size(); i++) {
    bfile.write((char *)&sizes[i], sizeof(size_t));
    bfile.write((char *)binaries[i], sizes[i]);
  }
}

cl::Program RbtOpenCL::loadBinariesFromCache(const std::string &hash) {
  std::ifstream bfile(getKernelCachePath(hash), std::ios::binary);
  if (!bfile)
    return cl::Program();
  size_t n;
  std::vector<char> buf;
  bfile.read((char *)&n, sizeof(size_t));
  buf.resize(n);
  bfile.read(buf.data(), n);
  cl::Program program(
      context, {device},
      cl::Program::Binaries(
          1, std::make_pair(static_cast<const void *>(buf.data()), n)));
  program.build({device});
  return program;
}

std::string RbtOpenCL::getKernelCachePath(std::string hash) {
  return "/tmp/" + hash + ".bin"; // TODO: move to proper folder
  // return GetRbtHome() + "/.openclCache/" + hash + ".bin";
}
