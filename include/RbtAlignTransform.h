//===-- RbtAlignTransform.h - Aligns ligand with principal axis -*- C++ -*-===//
//
// Part of the RxDock project, under the GNU LGPL version 3.
// Visit https//www.rxdock.org/ for more information.
// Copyright (c) 1998--2006 RiboTargets (subsequently Vernalis (R&D) Ltd)
// Copyright (c) 2006--2012 University of York
// Copyright (c) 2012--2014 University of Barcelona
// Copyright (c) 2019--2020 RxTx
// SPDX-License-Identifier: LGPL-3.0-only
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Aligns ligand with the principal axes of one of the active site cavities
///
//===----------------------------------------------------------------------===//

#pragma once

#include "RbtBaseBiMolTransform.h"
#include "RbtCavity.h"
#include "RbtRand.h"

class RbtAlignTransform : public RbtBaseBiMolTransform {
public:
  // Static data member for class type
  static std::string _CT;
  // Parameter names
  static std::string _COM;
  static std::string _AXES;
  RbtAlignTransform(const std::string &strName = "ALIGN");
  ~RbtAlignTransform() override;

protected:
  void SetupReceptor() override; // Called by Update when receptor is changed
  void SetupLigand() override;   // Called by Update when ligand is changed
  void
  SetupTransform() override; // Called by Update when either model has changed
  void Execute() override;

private:
  RbtAlignTransform(const RbtAlignTransform &) =
      delete; // Copy constructor disabled by default
  RbtAlignTransform &operator=(const RbtAlignTransform &) =
      delete; // Copy assignment disabled by default

protected:
private:
  RbtRand &m_rand; // keep a reference to the singleton random number generator
  RbtCavityList m_cavities;     // List of active site cavities to choose from
  std::vector<int> m_cumulSize; // Cumulative sizes, for weighted probabilities
  int m_totalSize;              // Total size of all cavities
};

typedef SmartPtr<RbtAlignTransform> RbtAlignTransformPtr;
